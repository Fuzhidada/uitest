package assertThrow;

public class BaseAssert {

    public String isOK(Boolean boo, String tips) {
        if (boo) {
            tips += "--->未发现问题";
        } else {
            tips += "--->数据异常";
        }
        return tips;
    }
}
