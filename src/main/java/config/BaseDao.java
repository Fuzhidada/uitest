package config;

import java.sql.Connection;
import java.sql.DriverManager;

public class BaseDao {
    public static final Connection getConn() {
        String driver = "oracle.jdbc.driver.OracleDriver";
        String passwrod = "bpmdb";
        String userName = "bpmdb";
        String url = "jdbc:oracle:thin:@192.168.103.88:1521:clamc";
        Connection connection = null;
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, passwrod, userName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static final void closeConn(Connection c) {
        try {
            if (c != null) {
                c.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


