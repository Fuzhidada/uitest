package config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;

public class BaseDriver {
    public final static Logger log = LoggerFactory.getLogger(BaseDriver.class);
    static WebDriver driver = null;


    public static final WebDriver getDriver() {
        try {


            System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
            ChromeDriverService service = new ChromeDriverService.Builder()
                    .usingDriverExecutable(
                            new File(
                                    "F:\\idea_workplace\\chromedriver.exe"))
                    .usingAnyFreePort().build();
            service.start();

            driver = new RemoteWebDriver(service.getUrl(), DesiredCapabilities.chrome());
            driver.navigate().to("http://192.168.103.12:8080/app/process/dayProcess");
            driver.manage().window().maximize();
            Thread.sleep(3000);

        } catch (Exception e) {
            log.error("basedriver出毛病了:" + e.getMessage());
            e.printStackTrace();
        }
        return driver;
    }

}



