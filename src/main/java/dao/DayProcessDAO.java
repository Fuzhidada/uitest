package dao;

import config.BaseDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DayProcessDAO extends BaseDao {

    Connection connection = null;
    PreparedStatement pre = null;
    ResultSet resultSet = null;

    public String getCircle1_0_(String sql) {
        {
            try {
                connection = getConn();
                pre = connection.prepareStatement(sql);
                resultSet = pre.executeQuery();
                while (resultSet.next()) {
                    return resultSet.getString(1);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeConn(connection);
            }
        }
        return "";
    }


}
