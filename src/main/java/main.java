import assertThrow.BaseAssert;
import config.BaseDriver;
import org.openqa.selenium.WebDriver;
import testcase.DayProcessTest;
import testcase.LoginTest;
import testcase.ProcessManagerTest;

public class main extends BaseDriver {
    private static WebDriver driver = getDriver();

    public static void main(String args[]) {
        try {
            BaseAssert anAssert = new BaseAssert();

            LoginTest loginTest = new LoginTest(driver);
            //正常登录
            loginTest.normalloginPageTest();
            Thread.sleep(3000);

            ProcessManagerTest processManagerTest = new ProcessManagerTest(driver);
            //点击左侧菜单
            Boolean t = processManagerTest.clickToDay();
            System.out.println(anAssert.isOK(t, "进入日间流程"));

            Thread.sleep(3000);
            DayProcessTest dayProcessTest = new DayProcessTest(driver);
            boolean t1 = dayProcessTest.circle0();
            System.out.println(anAssert.isOK(t1, "圆圈1三项检测"));

            System.out.println(anAssert.isOK(dayProcessTest.ClickCircle(), "点击第二个圆圈"));
            //延迟dom,便于页面加载完毕
            Thread.sleep(1000);

            boolean t3 = dayProcessTest.circle1();
            //校验 网银流水数量
            System.out.println(anAssert.isOK(t3, "圆圈2网银流水检测"));


            boolean click=dayProcessTest.matchCash();
            System.out.println(anAssert.isOK(click, "计算匹配结果+确认匹配"));

            Thread.sleep(6000);

            boolean confirMatch=dayProcessTest.circle2();
            System.out.println(anAssert.isOK(confirMatch, "进入节点3资金复核"));

            Thread.sleep(2000);

             boolean checkConfirm=dayProcessTest.conFirm();
            System.out.println(anAssert.isOK(checkConfirm, "--选择待复核数据"));

            Thread.sleep(2000);
            boolean conFirmClick=dayProcessTest.conFirmClick();
            System.out.println(anAssert.isOK(conFirmClick, "--确认复核"));


        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            //
        }
    }
}
