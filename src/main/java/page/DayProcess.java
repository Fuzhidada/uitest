package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;


/**
 * @author fuzhi
 * @describtion 日间收款流程
 * @date 2018-09-03
 */

public class DayProcess {

    public final static Logger log = LoggerFactory.getLogger(ProcessManager.class);
    private WebDriver driver;

    public DayProcess(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * @describtion 四个小圆圈
     */


    //圆圈1 当日收款计划 10/10
    public WebElement getCircle1_0() {
        return driver.findElement(By.xpath("//*[@id=\"app\"]/section/section/main/section/div/div[1]/div[1]/div/div[2]/div/ul/li[1]/div[1]/div[1]/span"));
    }

    public WebElement getCircle1_1() {
        return driver.findElement(By.xpath("/html/body/div[1]/section/section/main/section/div/div[1]/div[1]/div/div[2]/div/ul/li[1]/div[1]/div[2]/ul/li[2]"));
    }

    public WebElement getCircle1_2() {
        return driver.findElement(By.xpath("/html/body/div[1]/section/section/main/section/div/div[1]/div[1]/div/div[2]/div/ul/li[1]/div[1]/div[2]/ul/li[3]"));
    }

    //圆圈1 wei匹配计划展示框
    public WebElement getUnTotal() {
        return driver.findElement(By.xpath("/html/body/div[1]/section/section/main/section/div/div[2]/div[1]/div/div/div[2]/div[2]/div[1]/span[1]"));
    }

    //圆圈1 已匹配计划展示框
    public WebElement getnTotal() {
        return driver.findElement(By.xpath("/html/body/div[1]/section/section/main/section/div/div[2]/div[3]/div/div/div[2]/div[2]/div[1]/span[1]"));
    }

    //圆圈2
    public WebElement getCircle2() {
        return driver.findElement(By.xpath("//*[@id=\"app\"]/section/section/main/section/div/div[1]/div[1]/div/div[2]/div/ul/li[2]/div[1]/div[1]"));
    }

    //圆圈2--未匹配网银流水
    public WebElement getnSscInflow() {
        return driver.findElement(By.xpath("/html/body/div/section/section/main/section/div/div[1]/div[1]/div/div[2]/div/ul/li[2]/div[1]/div[2]/ul/li[3]"));
    }

    //未匹配网银流水table展示项
    public WebElement getnSscInflowTable() {
        return driver.findElement(By.xpath("/html/body/div[1]/section/section/main/section/div/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/span[1]"));
    }

    //圆圈2 未匹配计划  radio
    public WebElement getUnRadio() {

        return driver.findElement(By.xpath("/html/body/div[1]/section/section/main/section/div/div[2]/div[1]/div/div/div[2]/div[1]/div/div[3]/table/tbody/tr[7]/td[1]/div/label/span[1]/input"));
    }

    //圆圈2 未匹配网银流水 checkbox
    public List<WebElement> getUnInFlow() {
        List<WebElement> tr = driver.findElements(By.tagName("tbody")).get(2).findElements(By.tagName("tr"));
        int inFlownum = tr.size();//获取当前页的网银流水条数
        int mm = 0;
        if (inFlownum > 2) {
            mm = inFlownum / 2;
        } else {
            mm = inFlownum;
        }
        List<WebElement> tds = new LinkedList<WebElement>();
        for (int i = 0; i < mm; i++) {
            WebElement td = tr.get(i).findElement(By.tagName("td")).findElement(By.tagName("div")).findElement(By.tagName("div"))
                    .findElement(By.tagName("label")).findElements(By.tagName("span")).get(0).findElement(By.tagName("input"));
            tds.add(td);
        }
        return tds;
    }

    //计算匹配结果按钮
    public WebElement calcuButton() {
        return driver.findElement(By.xpath(" /html/body/div[1]/section/section/main/section/div/div[2]/div[3]/div/div/div[1]/div[2]/button"));
    }

    //确认匹配按钮
    public WebElement cmatchButton() {
        return driver.findElement(By.xpath("/html/body/div[1]/section/section/main/section/div/div[2]/div[3]/div/div/div[2]/div[3]/button"));
    }

    //资金复核圆圈
    public WebElement getCircle3_0() {
        return driver.findElement(By.xpath("/html/body/div[1]/section/section/main/section/div/div[1]/div[1]/div/div[2]/div/ul/li[3]"));
    }

    //待复核计划 随机选择几条复核

    public WebElement confirmMatch() {
        List<WebElement> trs = driver.findElement(
                By.xpath("/html/body/div/section/section/main/section/div/div[2]/div/div/div/div[2]/div[1]/div/div[3]/table/tbody"))
                .findElements(By.tagName("tr"));
        int trsize = trs.size();

        if (trsize > 0) {
            int random = (int) (Math.random() * trsize) ;
            return trs.get(random).findElements(By.tagName("td")).get(0).findElement(By.tagName("div"))
                    .findElement(By.tagName("label")).findElements(By.tagName("span")).get(0).findElement(By.tagName("input"));
        }
         return null;
    }

    //复核按钮
    public WebElement confirmButton() {
return  driver.findElement(By.xpath("/html/body/div[1]/section/section/main/section/div/div[2]/div/div/div/div[2]/div[2]/div[1]/button"));
    }


}

