package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Login {

    public final static Logger log = LoggerFactory.getLogger(Login.class);
    private WebDriver driver;

    public Login(WebDriver driver) {
        this.driver = driver;
    }

    /*账号密码 登录button*/
    public WebElement getUserNameInput() {
        return driver.findElement(By.xpath("/html/body/div/div/div/div[2]/div[2]/div[1]/input"));
    }

    public WebElement getPassWordInput() {
        return driver.findElement(By.xpath("/html/body/div/div/div/div[2]/div[2]/div[2]/input"));
    }

    public WebElement getLoginButton() {
        return driver.findElement(By.xpath("/html/body/div/div/div/div[2]/div[2]/button"));
    }

}
