package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author fuzhi
 * @describtion 流程管理
 * @date 2018-09-03
 */
public class ProcessManager {
    public final static Logger log = LoggerFactory.getLogger(ProcessManager.class);
    private WebDriver driver;

    public ProcessManager(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * 左侧日间收款流程菜单
     */
    public WebElement clickDayli() {
        return driver.findElement(By.xpath("/html/body/div/section/section/aside/div/div[2]/div[3]/div[3]/a"));
    }

}

