package testcase;

import dao.DayProcessDAO;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import page.DayProcess;

import java.util.List;

public class DayProcessTest {
    public final static Logger log = LoggerFactory.getLogger(DayProcessTest.class);
    private DayProcess dayProcess;
    private DayProcessDAO dayProcessDAO;
    private WebDriver driver;

    public DayProcessTest(WebDriver webDriver) {
        dayProcess = new DayProcess(webDriver);
        dayProcessDAO = new DayProcessDAO();
        this.driver = webDriver;
    }

    public boolean ClickCircle() {  //点第二个圆圈
        WebElement circle2 = dayProcess.getCircle2();
        circle2.click();
        return true;
    }


    /**
     * 圆圈1
     * 10/10
     * 未匹配
     * 已匹配
     * 只用获取一个圆圈
     */
    public Boolean circle0() {
        StringBuffer result = new StringBuffer();

        WebElement circle1_0 = dayProcess.getCircle1_0();
        WebElement circle1_1 = dayProcess.getCircle1_1();
        WebElement circle1_2 = dayProcess.getCircle1_2();
        String text1_0 = circle1_0.getText();
        String text1_1 = circle1_1.getText().split("：")[1];
        String text1_2 = circle1_2.getText().split("：")[1];

        String jdbcResult1 = dayProcessDAO.getCircle1_0_("select count(*) from ACT_RU_EXECUTION t" +
                " where t.business_key_!=' ' and length(t.business_key_)>21 ");                      //全部的
        String jdbcResult2 = dayProcessDAO.getCircle1_0_("select count(*) from ACT_RU_EXECUTION t " +
                "where t.business_key_!=' ' and length(t.business_key_)>21 and t.rev_ not in(3,4)"); //未匹配的
        String jdbcResult3 = dayProcessDAO.getCircle1_0_("select count(*) from ACT_RU_EXECUTION t" +
                " where t.business_key_!=' ' and length(t.business_key_)>21 and t.rev_=3  ");       //待复核的,已匹配
        String jdbcResult4 = dayProcessDAO.getCircle1_0_("select count(*) from ACT_RU_EXECUTION t" +
                " where t.business_key_!=' ' and length(t.business_key_)>21 and t.rev_=4  ");        //已复核 待确认


        WebElement circle0_unTotal = dayProcess.getUnTotal();
        String text_unTotal = circle0_unTotal.getText().substring(1, circle0_unTotal.getText().length()-1).trim(); //未匹配计划展示框的总条数  共 7 条
        if (text_unTotal != null && text_unTotal.equals(jdbcResult2)) {
            result.append("-------------圆圈未匹配数据与下方展示栏一致\n");
        } else {
            log.error(" 展示栏未匹配数据---"+ text_unTotal+"数据库数据未匹配---"+jdbcResult2+"\n");
        }

        WebElement circle0_nTotal = dayProcess.getnTotal();
        System.out.print(circle0_nTotal.getText());
        String text_nTotal = circle0_unTotal.getText().substring(1, circle0_unTotal.getText().length()-1).trim();//已匹配计划展示框的总条数  共 2条
        if (text_nTotal != null && text_nTotal.trim().equals(jdbcResult3)) {
            result.append("-----------------圆圈已匹配数据与下方展示栏一致\n");
        } else {
            log.error(" 展示栏已匹配数据" + text_nTotal+"\n");
        }


        if (text1_0 != null && text1_1 != null && text1_2 != null && text1_0.equals(jdbcResult1 + "/" + jdbcResult1) && text1_1.equals(jdbcResult2) && text1_2.equals(jdbcResult3)) {
            log.error("" + result);
            return true;
        }
        log.error(text1_0 + " " + text1_1 + "  "+text1_2 + " " + jdbcResult1 + " " + jdbcResult2 + " " + jdbcResult3);
        return false;
    }


    public Boolean circle1() {
        WebElement circle2_2 = dayProcess.getnSscInflow();
        String text2_2 = circle2_2.getText().split("：")[1]; //圆圈未匹配网银流水

        WebElement circle2_inFlowTable = dayProcess.getnSscInflowTable();
        String circle2_inFlowTableTable = circle2_inFlowTable.getText().substring(2, 3); //展示table未匹配网银流水

        String jdbcResult5 = dayProcessDAO.getCircle1_0_(" select count(*) from ssc_bus_inflow t " +
                "where t.blendplanid=' '  ");                                                              //未网银流水

        if (text2_2 != null && circle2_inFlowTableTable != null && text2_2.equals(jdbcResult5) && jdbcResult5.equals(circle2_inFlowTableTable)) {


            return true;
        } else {
            log.error(" " + text2_2 + " " + circle2_inFlowTableTable + " " + jdbcResult5);
            return false;
        }

    }

    public boolean matchCash() throws InterruptedException {
        try {
            WebElement input_radio = dayProcess.getUnRadio();      //由于有hidden属性 , 点击不到
            JavascriptExecutor js = (JavascriptExecutor) driver;

            js.executeScript("arguments[0].click();", input_radio); // 选中 一条未匹配收款计划

            List<WebElement> tds = dayProcess.getUnInFlow();
            for (WebElement td : tds) {
                js.executeScript("arguments[0].click();", td);       //选中 多条未匹配网银流水
            }

            WebElement calcuButton = dayProcess.calcuButton();
            calcuButton.click();                                          //计算匹配结果

            Thread.sleep(2000);
            WebElement cleMatch = dayProcess.cmatchButton();    //点击确认匹配按钮
            cleMatch.click();

        }catch (Exception e){
            log.error(e.getMessage());
            return false;
        }
return  true;

    }
    //点击节点3 资金复核 ---在随机选择几条大夫和数据
    public boolean circle2(){
        try{
      WebElement circle2=dayProcess.getCircle3_0();
      circle2.click();

        }catch (Exception e){
            log.error(e.getMessage());
             return  false;
        }
        return  true;
    }
    public boolean conFirm() {
    try{
        JavascriptExecutor js = (JavascriptExecutor) driver;

        WebElement check=dayProcess.confirmMatch();
        if(check!=null) {
            js.executeScript("arguments[0].click();", check);
            return true;
        }
    }catch (Exception e){
        return  false;
    }
        return  false;
    }
    public boolean conFirmClick() {
        try{
WebElement confirmButton=dayProcess.confirmButton();
confirmButton.click();
    return  true;
        }catch (Exception e){
            log.error("点击复核按钮出错"+e.getMessage());
        }
        return false;
    }
}
