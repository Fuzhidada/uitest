package testcase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import page.Login;
import testdata.LoginData;

public class LoginTest {
    private Login loginPage;
    private LoginData loginData;

    public LoginTest(WebDriver webDriver) {
        loginPage = new Login(webDriver);
        loginData = new LoginData();
    }

    public String normalloginPageTest() {
        WebElement username = loginPage.getUserNameInput();
        WebElement password = loginPage.getPassWordInput();
        WebElement loginButton = loginPage.getLoginButton();

        username.sendKeys(loginData.usernameData());
        password.sendKeys(loginData.passwordData());
        loginButton.click();
        return "success";
    }

}
