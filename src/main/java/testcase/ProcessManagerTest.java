package testcase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import page.ProcessManager;

public class ProcessManagerTest {
    private ProcessManager processManager;

    public ProcessManagerTest(WebDriver webDriver) {
        processManager = new ProcessManager(webDriver);
    }

    public boolean clickToDay() {
        try {
            WebElement dayProcessLi = processManager.clickDayli();
            dayProcessLi.click();
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
        return true;
    }
}
