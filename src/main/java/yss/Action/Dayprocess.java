/*
package yss.Action;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yss.BaseDaoImp.DaoImp;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Dayprocess extends DaoImp {
    public final static Logger log = LoggerFactory.getLogger(Dayprocess.class);
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    private Connection conn = getConn();

    public void dayProcess(WebDriver driver) throws SQLException {
        try {
            //收款菜单
            WebElement processLi = driver.findElement(By.className("second-class-nav"));
            //一级菜单和 二级菜单
            List<WebElement> mangerProcess = processLi.findElements(By.tagName("div"));
            List<WebElement> mangerProcess5 = mangerProcess.get(1).findElements(By.tagName("div"));
            for (int i = 0, len = mangerProcess5.size(); i < len; i++) {
                switch (i) {
                    case 0: //收款流程管理
                        break;
                    case 2:  //收款流程
                        //今日项目统计的圆圈
                        List<WebElement> sumToday = driver.findElements(By.tagName("tbody")).get(0).findElements(By.cssSelector("tr>td"));
                        for (int i0 = 0, len0 = sumToday.size(); i0 < len0; i0++) {
                            switch (i0) {
                                case 0:
                                    if (!"1".equals(sumToday.get(0).findElement(By.tagName("div")).getAttribute("innerHTML"))) {
                                        log.error(sumToday.get(0).findElement(By.tagName("div")).getAttribute("innerHTML") + "今日项目统计，日间流程序号错误");
                                    }
                                    break;
                                case 1:
                                    if (!sumToday.get(1).findElement(By.tagName("div")).getAttribute("innerHTML").equals("日间收款流程")) {
                                        log.error(sumToday.get(0).findElement(By.tagName("div")).getAttribute("innerHTML") + "今日项目统计，日间流程流程名称错误");
                                    }
                                    break;
                                case 2:
                                    List<WebElement> todayRecl = sumToday.get(2).findElements(By.cssSelector("div>ul>li"));
                                    for (int i00 = 0, len00 = sumToday.size(); i00 < len00; i00++){
                                        switch (i00) {
                                            case 0: //当日收款计划
                                                WebElement dayprocess1 = todayRecl.get(0).findElement(By.tagName("a"));
                                                //检验click   左侧 圆


                                                WebElement circle1 = dayprocess1.findElements(By.cssSelector("div>div")).get(0);
                                                log.info("circle1；" + circle1.getText());
                                                //  仅打印此信息，待后续开发
                                                //右侧统计项
                                                WebElement content1 = dayprocess1.findElements(By.className("circleProcess-right")).get(0);
                                                log.info(content1.getAttribute("class"));

                                                List<WebElement> content11 = content1.findElements(By.cssSelector("ul>li"));
                                                if (!"当日收款计划".equals(content11.get(0).getText())){
                                                    log.error("圆圈1当日收款计划文字不正确");}

                                                preparedStatement = conn.prepareStatement("select count(*) uncount from cle_bus_check where check_status=1 and re_check=1 ");
                                                resultSet = ((PreparedStatement) preparedStatement).executeQuery();
                                                int uncount = 0;
                                                while (resultSet.next()) {
                                                    uncount = resultSet.getInt("uncount");
                                                }
                                                log.info("未匹配数据：" + uncount);
                                                if (uncount != Integer.parseInt(content11.get(1).getText().split("：")[1])){
                                                    log.error("圆圈1未匹配数据不正确" + content11.get(1).getText());}

                                                preparedStatement = conn.prepareStatement("select count(*) ncount from cle_bus_check where check_status!=1 and re_check=3 ");
                                                resultSet = ((PreparedStatement) preparedStatement).executeQuery();
                                                int ncount = 0;
                                                while (resultSet.next()) {
                                                    ncount = resultSet.getInt("ncount");
                                                }
                                                log.info("已匹配数据：" + ncount);
                                                if (ncount != Integer.parseInt(content11.get(2).getText().split("：")[1])){
                                                    log.error("圆圈1已匹配数据不正确" + content11.get(2).getText());}


                                                break;
                                            case 1:
                                                //资金匹配
                                                WebElement dayprocess2 = todayRecl.get(4).findElement(By.tagName("a"));
                                                //检验click   左侧 圆


                                                WebElement circle2 = dayprocess2.findElements(By.cssSelector("div>div")).get(0);
                                                log.info("circle2；" + circle2.getText());
                                                //  仅打印此信息，待后续开发
                                                //右侧统计项
                                                WebElement content2 = dayprocess2.findElements(By.className("circleProcess-right")).get(0);
                                                log.info(content2.getAttribute("class"));

                                                List<WebElement> content22 = content2.findElements(By.cssSelector("ul>li"));
                                                log.info("资金匹配" + content22.get(0).getText());
                                                if (!"资金匹配".equals(content22.get(0).getText()))
                                                    log.error("圆圈2资金匹配文字不正确");

                                                preparedStatement = conn.prepareStatement("select count(*) uncount from cle_bus_check where check_status=1 and re_check=1 ");
                                                resultSet = (preparedStatement).executeQuery();
                                                int uncount2 = 0;
                                                while (resultSet.next()) {
                                                    uncount2 = resultSet.getInt("uncount");
                                                }
                                                log.info("未匹配收款计划：" + uncount2);
                                                if (uncount2 != Integer.parseInt(content22.get(1).getText().split("：")[1]))
                                                    log.error("圆圈2未匹配收款计划" + content22.get(1).getText());

                                                preparedStatement = conn.prepareStatement("select count(*) ncount from ssc_bus_inflow where blendplanid=' '");
                                                resultSet = (preparedStatement).executeQuery();
                                                int ncount2 = 0;
                                                while (resultSet.next()) {
                                                    ncount2 = resultSet.getInt("ncount");
                                                }
                                                log.info("未匹配网银流水：" + ncount2);
                                                if (ncount2 != Integer.parseInt(content22.get(2).getText().split("：")[1]))
                                                    log.error("圆圈2未匹配网银流水不正确" + content22.get(2).getText());

                                                break;
                                            case 2:
//
                                                //收款收息流程
                                                WebElement dayprocess3 = todayRecl.get(8).findElement(By.tagName("a"));
                                                //检验click   左侧 圆


                                                WebElement circle3 = dayprocess3.findElements(By.cssSelector("div>div")).get(0);
                                                log.info("circle3；" + circle3.getText());
                                                //  仅打印此信息，待后续开发
                                                //右侧统计项
                                                WebElement content3 = dayprocess3.findElements(By.className("circleProcess-right")).get(0);
                                                log.info(content3.getAttribute("class"));

                                                List<WebElement> content33 = content3.findElements(By.cssSelector("ul>li"));
                                                log.info("收款收息流程" + content33.get(0).getText());
                                                if (!"收款收息流程".equals(content33.get(0).getText()))
                                                    log.error("圆圈3-收款收息流程-文字不正确");

                                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                                String time = simpleDateFormat.format(new Date());

                                                preparedStatement = conn.prepareStatement("select count(*) uncount from cle_bus_check where check_status！=1 and re_check=2 and balance_date=to_date(?,'yyyy-MM-dd')");
                                                preparedStatement.setString(1, time);
                                                resultSet = preparedStatement.executeQuery();
                                                int uncount3 = 0;
                                                while (resultSet.next()) {
                                                    uncount3 = resultSet.getInt("uncount");
                                                }
                                                log.info("已复核：" + uncount3);
                                                if (uncount3 != Integer.parseInt(content33.get(1).getText().split("：")[1]))
                                                    log.error("圆圈3已复核不正确" + content33.get(1).getText());

                                                preparedStatement = conn.prepareStatement("select count(*) ncount from cle_bus_check where check_status！=1 and re_check=3");
                                                resultSet = preparedStatement.executeQuery();
                                                int ncount3 = 0;
                                                while (resultSet.next()) {
                                                    ncount3 = resultSet.getInt("ncount");
                                                }
                                                log.info("待复核：" + ncount3);
                                                if (ncount3 != Integer.parseInt(content33.get(2).getText().split("：")[1]))
                                                    log.error("圆圈待复核不正确" + content33.get(2).getText());
                                                break;
                                        }
                                    }

                            }
                        }


                        break; //流程管理 end

                    case 4: //日间
                        log.info("日间:" + mangerProcess5.get(4).getText());
                        mangerProcess5.get(4).click();
                        Thread.sleep(3000);//页面加载 时间
//                        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);


                        //校验未匹配收款计划数据
                        //明细品种+ 输入框模糊查询
                        //数据
                        List<WebElement> unmatchPlan = driver.findElements(By.tagName("tbody")).get(0).findElements(By.tagName("tr"));
                        for (int i1 = 0, len1 = unmatchPlan.size(); i1 < len1; i1++) {
//                            System.out.println("kaskdaskdj" + unmatchPlan.get(i1).getText());
                        }
                        preparedStatement = conn.prepareStatement("select * from cle_bus_check where rownum>=? and rownum<=? ");
                        preparedStatement.setInt(1, 0);
                        preparedStatement.setInt(2, 10);
                        int count = 0;
                        ResultSet resultSet = preparedStatement.executeQuery();
                        while (resultSet.next()) {
                            count++;
                        }
                        if (unmatchPlan.size() != count) {
                            log.error("未匹配数据不相符");
                        }


                        break;
                    case 6:
                        break;
                    case 8:
                        break;
                }
            }


        } catch (Exception e) {

            e.printStackTrace();
            log.error(e.getMessage());

        } finally {
            conn.close();
            log.error("流程管理--当日收款计划--统计项测试完毕");
            driver.quit();
        }
    }
}
*/
